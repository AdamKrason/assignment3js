import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { StorageUtil } from 'src/app/utils/storage.util';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue/pokemon-catalogue.service';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { environment } from 'src/environments/environment';
const {apiKey} = environment

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage {
  constructor(
    private readonly router: Router,
    private loginService : LoginService
  ) { }

  logout(): void {
    StorageUtil.storageDelete(apiKey)
    this.loginService.id = -1;
    this.loginService.username = "";
    this.loginService.pokemon = [];
    this.router.navigateByUrl("/login");
  }

  pokemons? : Pokemon[] = this.loginService.getUser.pokemon

}
