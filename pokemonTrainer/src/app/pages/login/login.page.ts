import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageUtil } from 'src/app/utils/storage.util';
import { environment } from 'src/environments/environment';

const { apiKey } = environment
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit{

  constructor( private readonly router: Router) { }

  handleLogin(): void {
    console.log("This should route");
    
    this.router.navigateByUrl("/catalogue");
  }

  /**
   * If the user has already logged in, then redirect them to the catalogue page
   */
  ngOnInit(): void {
    if(localStorage.getItem(apiKey) != undefined) {
      this.router.navigateByUrl("/catalogue");
    }
  }

}
