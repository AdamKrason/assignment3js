import { Component } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  get user(): User | undefined {
    return this.loginService.getUser;
  }

  constructor(
    private readonly loginService: LoginService
  ) { }

  ngOnInit(): void {
  }
}
