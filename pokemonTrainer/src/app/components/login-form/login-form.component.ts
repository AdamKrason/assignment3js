import { Component, EventEmitter, Output } from '@angular/core';
import { NgForm } from '@angular/forms'
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { environment } from 'src/environments/environment';

const { apiKey,apiUsers } = environment
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css'],
})
export class LoginFormComponent {

  @Output() login: EventEmitter<void> = new EventEmitter();


  constructor(
    private readonly loginService: LoginService,
    private readonly router:Router
    ) { }

  userPokemon? : string[];

  public loginSubmit(loginForm: NgForm): void {

    const { username } = loginForm.value;
    
    this.loginService.login(username).subscribe((data) => {
      console.log("READING USER: " + data.length  + " = length")

      if (data.length > 0) {
        // add state to service
        this.loginService.id = data[0].id;
        this.loginService.username = data[0].username
        this.loginService.pokemon = data[0].pokemon
        console.log("user found" );
        
        localStorage.setItem(apiKey, data[0].username);

      } else {
        this.loginService.createUser(username).subscribe((data) => {
          console.log("user not found")
          this.loginService.id = data.id;
          this.loginService.username = data.username
          this.loginService.pokemon = data.pokemon
          // add state to service
          localStorage.setItem(apiKey, data.username);
          this.router.navigateByUrl("/catalogue");
        });
      }

      this.router.navigateByUrl("/catalogue");
      
      console.log(this.loginService.getUser)
    });
  }

}
