import { Component, Injectable, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LoginService } from 'src/app/services/login.service';

const {apiUsers,apiKey} = environment

@Component({
  selector: 'app-pokemon-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css']
})

@Injectable({
  providedIn: "root"
})

export class PokemonListItemComponent implements OnInit {
  constructor(
    private loginService : LoginService,
    private readonly http : HttpClient
    
  ) { }
  
  @Input() pokemon?: Pokemon;

  get user(): User {
    return this.loginService.getUser!;
  }


  ngOnInit(): void {
  }

  test () {
    this.addPokemon().subscribe((data) => {
      console.log("NEW USER:" )
      console.log(data);
    })
  }

  addPokemon() : Observable<User> {
    let tempArr :Pokemon[] = [];

    this.user.pokemon.push(this.pokemon!)
    for (let i = 0; i < this.user.pokemon.length; i++) {
      tempArr.push(this.user.pokemon[i]);
    }

    tempArr.push(this.pokemon!);
    // alert(`You have caught ${this.pokemon?.name!}`)

    console.log(this.user.pokemon)

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    const tempUser = {
      id: this.user.id,
      username: this.user.username,
      pokemon: tempArr
    }
    
    return this.http.patch<User>(`${apiUsers}/${this.user?.id!}`,tempUser,{headers})
  }

}