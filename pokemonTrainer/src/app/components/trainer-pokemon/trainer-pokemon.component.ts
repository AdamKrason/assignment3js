import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Pokemon } from 'src/app/models/pokemon.model';
import { User } from 'src/app/models/user.model';
import { LoginService } from 'src/app/services/login.service';
import { environment } from 'src/environments/environment';

const {apiUsers,apiKey} = environment

@Component({
  selector: 'app-trainer-pokemon',
  templateUrl: './trainer-pokemon.component.html',
  styleUrls: ['./trainer-pokemon.component.css']
})
export class TrainerPokemonComponent implements OnInit {

  constructor(
    private loginService : LoginService,
    private readonly http : HttpClient
    
  ) { }
  
  @Input() pokemon?: Pokemon;

  get user(): User {
    return this.loginService.getUser!;
  }

  ngOnInit(): void {
    // throw new Error('Method not implemented.');
    console.log(typeof this.pokemon);
  }

  test() {
    this.removeTrainerPokemon().subscribe((data) => {

    });
  }

  removeTrainerPokemon():Observable<User>{
    let tempArr : Pokemon[] = [];
    for (let i = 0; i < this.loginService.getUser.pokemon.length; i++) {
      if (this.loginService.getUser.pokemon[i].name != this.pokemon?.name) {
        tempArr.push(this.loginService.getUser.pokemon[i]);
      }
    }
    this.loginService.pokemon = tempArr
    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    const tempUser = {
      id: this.loginService.getUser.id,
      username: this.loginService.getUser?.username,
      pokemon: tempArr
    }

    console.log("PATCHING: " + apiUsers)

    this.pokemon = undefined;

    return this.http.patch<User>(`${apiUsers}/${this.user?.id!}`,tempUser,{headers})
  }
}
