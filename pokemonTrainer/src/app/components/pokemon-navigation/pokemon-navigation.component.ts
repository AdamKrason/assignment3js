import { Component, OnInit } from '@angular/core';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue/pokemon-catalogue.service';
import { environment } from 'src/environments/environment';

const { apiPokemon } = environment

@Component({  
  selector: 'app-pokemon-navigation',
  templateUrl: './pokemon-navigation.component.html',
  styleUrls: ['./pokemon-navigation.component.css']
})
export class PokemonNavigationComponent {

  constructor(private pokemonCatalogueService: PokemonCatalogueService) { }
  
  private _numberOfPokemons = Number(apiPokemon.match(/[^limit=]*$/))
  private _totalPages = Math.ceil(this._numberOfPokemons/this.pokemonCatalogueService.pokemonPerPage)

  totalPages(): number{
    return this._totalPages;
  }
  
  currentPage(): number {
    return this.pokemonCatalogueService.pokeIndex/this.pokemonCatalogueService.pokemonPerPage + 1
  }

  /**
   * If the current page is greater than 54, return 54 + the difference between the index and 5.
   * Otherwise, if the current page is greater than or equal to 5, return the current page + the
   * difference between the index and 5. Otherwise, return the index
   * @param {number} index - the index of the page number in the array of page numbers
   * @returns The index of the page that is being shown.
   */
  showPage(index: number): number {
    const diff = index - 5
    if(this.currentPage() > this._totalPages-4) {
      return this._totalPages-4 + diff
    } else if(this.currentPage() >= 5) {
      return this.currentPage() + diff
    }
    return index
  }

  hasNext(): boolean {
    if (this.currentPage() < this._totalPages) {
      return true
    }
    return false
  }
  
  hasPrev(): boolean {
    if (this.pokemonCatalogueService.pokeIndex >= this.pokemonCatalogueService.pokemonPerPage) {
      return true
    }
    return false
  }

  /**
   * It checks if there is a next page of pokemons to be fetched, and if so, it scrolls to the top of
   * the page and fetches the next page of pokemons
   */
  onNextClick(): void {
    if(this.hasNext()) {
      window.scroll({ 
        top: 0, 
        left: 0, 
        behavior: 'smooth' 
      });
      this.pokemonCatalogueService.findNextPokemons()
    }
  }

  /**
   * If there is a previous page, scroll to the top of the page and call the findPreviousPokemons()
   * function from the PokemonCatalogueService
   */
  onPrevClick(): void {
    if(this.hasPrev()) {
      window.scroll({ 
        top: 0, 
        left: 0, 
        behavior: 'smooth' 
      });
      this.pokemonCatalogueService.findPreviousPokemons()
    }
  }

  /**
   * It returns true if the page number passed in is the same as the current page number
   * @param {number} page - number - the page number that we're checking
   * @returns A boolean value.
   */
  isCurrentPage(page: number): boolean {
    return page == this.currentPage()
  }

  /**
   * It scrolls to the top of the page and then calls the findPokemonsOnPage function in the
   * PokemonCatalogueService
   * @param {number} page - number - the page number that the user clicked on
   */
  goToPageOnClick(page: number): void {
    window.scroll({ 
      top: 0, 
      left: 0, 
      behavior: 'smooth' 
    });
    this.pokemonCatalogueService.findPokemonsOnPage(page)
  }
}