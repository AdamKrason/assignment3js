import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';

const { apiUsers, apiKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private _user?: User = ({
    id: -1,
    username: "",
    pokemon: []
  });

  // private _pokemons: Pokemon[] = [];

  constructor(private readonly http: HttpClient) { }

  set username(name:string) {
    this._user!.username = name
  }

  set id(id:number) {
    this._user!.id = id
  }

  set pokemon(pokemon:Pokemon[]) {
    this._user!.pokemon = pokemon;
  }

  get getUser() : User {
    return this._user!;
  }

  public login(username: string): Observable<User[]> {
    return this.http.get<User[]>(`${apiUsers}?username=${username}`)
  }



  // Create user
  public createUser(username: string): Observable<User> {
    const user = {
      username,
      pokemon: []
    };

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });
 
    return this.http.post<User>(apiUsers, user, {
      headers
    });
  }
}
