import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from '@angular/common';
import { LoginPage } from "./pages/login/login.page";
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { AuthGuard } from './guards/auth.guard';
import { TrainerPage } from './pages/trainer/trainer.page';



const routes: Routes = [
  {
      path:"",
      pathMatch:"full",
      redirectTo: "/login"
  },
  {
      path: "login",
      component: LoginPage
  },
  {
      path: "catalogue",
      component: CataloguePage,
      canActivate: [AuthGuard]
  },
  {
    path: "trainer",
    component: TrainerPage,
    canActivate: [AuthGuard]
  }


]


@NgModule({
  imports: [
      RouterModule.forRoot(routes)
  ],
  exports: [
      RouterModule
  ]
})

export class AppRoutingModule { }
