import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { environment } from 'src/environments/environment';

const {apiKey} = environment

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private readonly router: Router,
  ) {

  }

  canActivate(){
      if (localStorage.getItem(apiKey) && localStorage.getItem(apiKey) != undefined) {
        return true;
      }else {
        this.router.navigateByUrl("/login");
        return false;
      }



  }
  
}
