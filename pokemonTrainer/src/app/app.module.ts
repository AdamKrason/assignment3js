import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { LoginPage } from './pages/login/login.page';
import { AppRoutingModule } from './app-routing.module';
import { CataloguePage } from './pages/catalogue/catalogue.page';
import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
import { FormsModule } from '@angular/forms';
import { PokemonNavigationComponent } from './components/pokemon-navigation/pokemon-navigation.component';
import { TrainerPage } from './pages/trainer/trainer.page';
import { TrainerPokemonComponent } from './components/trainer-pokemon/trainer-pokemon.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginPage,
    LoginFormComponent,
    CataloguePage,
    PokemonListComponent,
    PokemonListItemComponent,
    PokemonNavigationComponent,
    TrainerPage,
    TrainerPokemonComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
